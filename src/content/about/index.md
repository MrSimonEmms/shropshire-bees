---
title: About
subtitle: Shropshire Beekeepers Association is one of the oldest beekeeping associations in the country
image: /img/pages/about/IMG_20220312_121329.jpg
imageLarge: true
order: 0
---


## Wildlife

The Conduit Head site covers approximately 1.5 acres with a wide variety of micro-environments, or ecosystems which have been developed from the overgrown state when the site was taken over by SBKA. The ambition is to demonstrate beekeeping on the site but to maintain different wildlife habitats for the education and enjoyment of all visitors.

Today we have wildflower meadows, an orchard of heritage varieties, woodland and hedgerows, a marshy area with a pond and borehole fed stream, and garden vegetable and flower beds. All of which mean a wide variety of plants and animals can be found on site.

To further encourage wildlife to the site we have a multitude of bird boxes to encourage, amongst others - owls, swifts, flycatchers as well as the more familiar tit boxes. We have built a bird hide for viewing birds coming to our feeding station. We also have installed boxes for the local population of bats, homes for hedgehogs and solitary bees. All something to look out for when visiting the Conduit Head site.

The pond and stream have sticklebacks and trout as well as frogs, toads and newts and a variety of water plants including watercress, water mint and flag irises. Damselflies and dragonflies are often seen around the site, together with moorhens, coots and visiting mallard.

## Water

The River Severn was the main water source for Shrewsbury from the time the settlement was established but, as the town grew, the river became polluted and there were frequent outbreaks of typhoid and, in the 1830’s, cholera. In 1555, following one typhoid outbreak, the town burghers were given a royal decree by Queen Elizabeth I to provide a pure water source for the people of Shrewsbury. Nine wells were dug at the Broadwells site to intercept the natural springs. Each well is about 2.6m (9 feet) deep penetrating the clay to glacial gravels below. The wells at Conduit Head are artesian and flow all year.

Water was supplied to Shrewsbury from circa 1556, flowing by gravity in wooden pipes, under the river into several cisterns or water tanks, from where it was distributed, in buckets, by water carriers. In 1578, the (now Grade II listed) cistern house was built to collect water from the wells.

In the early 1800’s, the Shrewsbury Waterworks Company laid pipes to cast iron conduits in the town streets from which residents could draw water. The Broadwells site then became known as Conduit Head. There are several conduits still on Shrewsbury’s streets, but they are no longer in use. In the early 1870’s the company was bought by the Corporation of Shrewsbury.

In 1908 a 9 metre (30 feet) well was dug and a pump house built. A gas engine pumped the water up to a water tower on Nobold lane, feeding the Bayston Hill area. Use of the site for public water supply ceased in 1947 due to concerns over contamination as the site was too small and uneconomic to improve the chlorination system.

In 1956 a 38 metre (124 feet) deep well was drilled as a civil defence initiative to assess the availability of water in the event of reservoirs and rivers being contaminated by fallout in the event of a nuclear war.

In 1987 the Shrewsbury Civic Society and Severn Trent Water Authority commissioned a series of viewing platforms around the wellheads, and a visitor centre was opened. The site was vandalised and then abandoned until the Shropshire Beekeepers Association took over the site in 2017.
