---
title: Learn Beekeeping
subtitle: Beekeeping is a fantastic hobby to learn. We run a variety of courses for absolute beginners to gain the confidence and knowledge to look after their first colonies.
image: /img/pages/index/beekeeping.jpg
imageLarge: true
imagePosition: top center
---

Beekeeping is a rewarding and fascinating activity.

Shropshire Beekeepers Association runs various courses throughout the year to
learn the theory and practical skills required to successfully manage your own
colonies.

Beekeeping involves a significant investment in time and money, especially when
starting out. For that reason, it is recommended that you start by doing the
[Taster Day](#taster-day) to gauge whether beekeeping is for you. If it is, the
next stage is to embark upon a [Beginners Course](#beginners-course).

## Taster day

**Saturday 14th September 2024, 10:00-16:00**

Beekeeping is a wonderful hobby where there is always more to learn. Even beekeepers
with decades of experience have more to find out about this fascinating insect.

The annual Shropshire Bees Taster Day is a great way of finding out if beekeeping
is for you. Over the course of the day, you’ll get an overview of the theory of
beekeeping and also the chance to handle the bees under the supervision of our
experienced members.

This full day event costs £75 per person and is held at our apiary in Shrewsbury.

<v-btn color="primary" large href="https://www.eventbee.com/v/beekeeping-taster-day-2024/event?eid=271819261">Book now</v-btn>

## Beginners course

~**Saturday course:** Saturday 1st June 2024, 10:30-16:30 for 3 weeks~ **SOLD OUT**

~**Evening course:** Wednesday 17 July 2024, 18:30-20:30 for 6 weeks~ **SOLD OUT**

The two courses offer the same content, but held at different times to make them as
accessible as possible for attendees. You can do it as a 3 full day sessions or 6
two hour sessions.

As the Saturday sessions are held earlier in the season, there will be enough time
for you to get your first colony of bees this year. With the Wednesday sessions being
slightly later, it is recommended that you get your own bees the following spring.

These courses are £125 per person and are held at our apiary in Shrewsbury.

<v-btn color="primary" large href="https://www.eventbee.com/v/beginners-beekeeping-course-2024-saturdays/event?eid=251619361">Saturdays</v-btn>

<v-btn color="primary" large href="https://www.eventbee.com/v/beginners-beekeeping-course-2024-evenings/event?eid=251019464">Evenings</v-btn>

## Mentoring

Once you have completed a course, we offer a mentoring programme for course graduates.
These are designed as a place to get encouragement and expertise from experienced
beekeepers.

As part of your Shropshire Bees membership, you are entitled and encouraged to join
one of our Bee Teams to help manage the bees in our apiary. This is a great opportunity
to gain experience of working with different bees and different beekeepers.

---

## Members Training

> Important. These events are only available to current SBKA members.

#### BBKA Basic Assessment

The BBKA Basic Assessment is for all beekeepers who have kept at least one colony of bees for a minimum of 12 months.

This course is designed to formalise the knowledge that you have gained whilst managing your first colony. It is a prerequisite for more demanding assessments and certifications.

There is no charge for the training, but the assessment fee is £30.

**IMPORTANT**

This is a 4 week course. All sessions will be held at the same location (the SBKA apiary in Nobold Lane, Shrewsbury) from 15th May - 19th June, 6:30PM - 8:30PM. Please arrive promptly for the start. The assessment will be held in August.

<v-btn color="primary" large href="https://www.eventbee.com/v/bbka-basic-assessment-2024/event?eid=272512114">Book</v-btn>

**IMPORTANT**

This is a 6 week course. All sessions will be held at the same location (the SBKA apiary in Nobold Lane, Shrewsbury) from 13th September - 15th November, 6:30PM - 8:30PM. Please arrive promptly for the start. The assessment will be held in November.

<v-btn color="primary" large href="https://www.eventbee.com/v/tutored-study-group-for-bbka-module-3-honey-bee-health/event?eid=231419165">Book</v-btn>

---

#### Refund Policy

Cancellations made 30 days or more before the event will be eligible for a full refund.

Cancellations made less than 30 days before the event will be eligible for a full refund on condition that we are able to resell the place.
