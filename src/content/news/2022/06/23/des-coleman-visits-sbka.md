---
title: Des Coleman Visits SBKA
# subtitle: We recently welcomed Des Coleman from ITV Central
image: /img/pages/news/descoleman.jpg
imageLarge: true
submenu: false
date: 2022-06-23
---

We recently welcomed Des Coleman ITV Central News to the association to show the work we've been doing.

<!--more-->

Visitors are always welcome on Wednesday and Saturday mornings and you too can see our apiary, wildlife sanctuary and historic wells.

<div class="text-center">
  <iframe src="https://www.facebook.com/plugins/video.php?height=367&href=https%3A%2F%2Fwww.facebook.com%2Fdroneairsolutionsltd%2Fvideos%2F878859766403732%2F&show_text=false&width=560&t=0" width="560" height="367" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe>


  Footage by [Drone Air Solutions Ltd](https://www.facebook.com/watch/?v=878859766403732)
</div>

