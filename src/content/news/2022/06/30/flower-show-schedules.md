---
title: Flower Show Schedules Published
image: /img/pages/news/flowershowbig.jpg
imageLarge: true
submenu: false
date: 2022-06-30
---

The schedule of classes for entries into the Bees, Honey and Wine section of the Shrewsbury Flower Show 2022 have been published.

<!--more-->

- <a href="/docs/flower-show-2022/schedule.pdf" target="_blank">Schedules</a>
- <a href="/docs/flower-show-2022/entry-form.pdf" target="_blank">Entry Form</a>
- <a href="/docs/flower-show-2022/childrens-schedule.pdf" target="_blank">Children's Schedules</a>
- <a href="/docs/flower-show-2022/childrens-entry.pdf" target="_blank">Children's Entry Form</a>
