---
title: May 20th is World Bee Day
image: /img/pages/news/alexas_fotos-78Zt_X5VcmU-unsplash.jpg
imageLarge: true
draft: false
submenu: false
date: 2022-05-15
---

In celebration of World Bee Day, we are opening up our apiary in Shrewsbury to the public.

<!--more-->

May 20th is World Bee Day - recognising the importance of pollinators to mankind, and the risks that they face.

We are opening the apiary in Shrewsbury with guided tours of the historic site and audio-visual presentations. There will also be the opportunity to get up close to the bees, both inside the beehive and under the microscope.

Open 10am-4pm, car parking and refreshments available.
