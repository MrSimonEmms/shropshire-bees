// eslint-disable-next-line import/extensions,import/no-unresolved
import { IContentDocument } from '@nuxt/content/types/content';

export interface ContentDoc extends IContentDocument {
  subtitle?: string;
  displayToc?: boolean;
  hideUpdated?: boolean;
  draft?: boolean;
  textColor?: string;
  image?: string;
  imagePosition?: string;
  imageLarge?: boolean;
  socialImage?: string;
  layout?: string;
}
