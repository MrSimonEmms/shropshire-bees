import { DateObjectUnits } from 'luxon';

export interface ICalendarEvent {
  name: string;
  start: Date;
  end: Date;
  location?: string[];
  description?: string;
  textColor?: string;
  timed?: boolean; // Set to "false" if all day event
  color?: string;
}

export interface ICalendarRepeats {
  name: string;
  location?: string[];
  startTime: DateObjectUnits;
  endTime: DateObjectUnits;
  startDate?: Date;
  endDate?: Date;
  description?: string;
  days?: number[];
  repeat?: 'LAST_TUESDAY_OF_MONTH';
  timed?: boolean;
  color?: string;
  textColor?: string;
  except?: Date[];
}
