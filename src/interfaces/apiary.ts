export interface IApiaryOpening {
  day: 'Monday' | 'Tuesday' | 'Wednesday' | 'Thursday' | 'Friday' | 'Saturday' | 'Sunday';
  openTime?: string;
  closeTime?: string;
}
