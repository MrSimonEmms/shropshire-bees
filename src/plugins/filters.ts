import { Vue } from 'nuxt-property-decorator';
import filters from '../filters';

export default () => {
  Object.keys(filters).forEach((key) => {
    const filter: Function = (filters as any)[key];

    Vue.filter(key, filter);
  });
};
