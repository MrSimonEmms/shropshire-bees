import { Module, VuexModule } from 'vuex-module-decorators';
import { IApiaryOpening } from '@/src/interfaces/apiary';

@Module({
  stateFactory: true,
  namespaced: true,
})
export default class Apiary extends VuexModule {
  apiaryAddress: string[] = [
    'Shropshire Beekeepers Association',
    'Nobold Lane',
    'Shrewsbury',
    'SY5 8NP',
  ];

  apiaryOpenHours: IApiaryOpening[] = [
    { day: 'Monday' },
    { day: 'Tuesday' },
    {
      day: 'Wednesday',
      openTime: '09:30',
      closeTime: '12:30',
    },
    { day: 'Thursday' },
    { day: 'Friday' },
    {
      day: 'Saturday',
      openTime: '09:30',
      closeTime: '12:30',
    },
    { day: 'Sunday' },
  ];

  charityNumber = '1164389';

  googleMap = 'https://g.page/shropshire-bees?share';

  w3w = 'lunch.crazy.retire';

  get address(): string[] {
    return this.apiaryAddress;
  }

  get charitableRegistrationNumber(): string {
    return this.charityNumber;
  }

  get mapUrl(): string {
    return this.googleMap;
  }

  get openingHours(): IApiaryOpening[] {
    return this.apiaryOpenHours;
  }

  get what3Words(): string {
    return this.w3w;
  }
}
