import { Module, VuexModule } from 'vuex-module-decorators';

@Module({
  stateFactory: true,
  namespaced: true,
})
export default class Social extends VuexModule {
  socials = [
    {
      title: 'Shropshire Beekeepers Association on Twitter',
      icon: 'mdi-twitter',
      link: 'https://twitter.com/ShropsBKA',
    },
    {
      title: 'Shropshire Beekeepers Association on Facebook',
      icon: 'mdi-facebook',
      link: 'https://facebook.com/shropshirebeekeepersassociation',
    },
  ];

  get list() {
    return this.socials;
  }
}
