import { Module, MutationAction, VuexModule } from 'vuex-module-decorators';
import { ILink } from '@/src/interfaces/router';

@Module({
  stateFactory: true,
  namespaced: true,
})
export default class Menu extends VuexModule {
  display: boolean = false;

  menu: ILink[] = [
    {
      title: 'About',
      to: {
        name: 'all',
        params: {
          pathMatch: 'about',
        },
      },
    },
    {
      title: 'Join',
      to: {
        name: 'all',
        params: {
          pathMatch: 'join',
        },
      },
    },
    {
      title: 'Learn',
      to: {
        name: 'all',
        params: {
          pathMatch: 'learn',
        },
      },
    },
    {
      title: 'Swarms',
      to: {
        name: 'all',
        params: {
          pathMatch: 'swarms',
        },
      },
    },
    // {
    //   title: 'Members',
    //   to: {
    //     name: 'all',
    //     params: {
    //       pathMatch: 'members',
    //     },
    //   },
    // },
  ];

  get list() {
    return this.menu;
  }

  get isOpen() {
    return this.display;
  }

  @MutationAction
  // eslint-disable-next-line class-methods-use-this
  async setDisplay(display: boolean) {
    return { display };
  }
}
