// eslint-disable-next-line import/no-extraneous-dependencies
import { NuxtConfig } from '@nuxt/types';
// eslint-disable-next-line import/extensions,import/no-unresolved
import { IContentDocument } from '@nuxt/content/types/content';
import sha256 from 'crypto-js/sha256';
import * as uuid from 'uuid';
import pkg from './package.json';

const projectName = 'Shropshire Beekeepers Association';
const domainName: string = process.env.DOMAIN_NAME ?? 'http://localhost:3000';
const defaultImage = '/img/pages/index/beekeeping.jpg';
const membersPasswordEnvvar = process.env.MEMBERS_PASSWORD;
if (!membersPasswordEnvvar) {
  throw new Error('No members password envvar set');
}

// This is not cryptographically secure, but it's good enough for our purposes
const salt = uuid.v4();
const hash = sha256(`${membersPasswordEnvvar}${salt}`).toString();
const membersPassword = [hash, salt].join(':');

const config: NuxtConfig = {
  srcDir: './src',
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: `%s | ${projectName}`,
    meta: [
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: `${domainName}${defaultImage}`,
      },
    ],
  },

  ssr: true, // Required so can update head properties

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/css/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/plugins/analytics.client.js',
    },
    {
      src: '~/plugins/vuetify.ts',
    },
    {
      src: '~plugins/filters.ts',
    },
    {
      src: '~plugins/persistedState.ts',
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://vuetifyjs.com/en/getting-started/installation/#nuxt-install
    [
      '@nuxtjs/vuetify',
      {
        treeShake: {
          components: ['VBtn', 'VIcon', 'VSimpleTable'],
        },
      },
    ],
    // https://github.com/nuxt-community/date-fns-module
    ['@nuxtjs/date-fns', { format: 'yyyy-MM-dd' }],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    icon: {
      source: './src/static/img/logo.png',
    },
    manifest: {
      lang: 'en',
      name: projectName,
      short_name: projectName,
    },
    meta: {
      name: projectName,
      ogHost: domainName,
      ogImage: defaultImage,
      twitterCard: 'summary_large_image',
      twitterSite: '@ShropsBKA',
    },
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  env: {
    DOMAIN_NAME: domainName,
    MEMBERS_PASSWORD: membersPassword,
    npm_package_description: pkg.description,
    googleAnalytics: `${process.env.GOOGLE_ANALYTICS}`,
  },

  generate: {
    async routes() {
      const { $content } = await import('@nuxt/content');
      const files = await $content({ deep: true }).only(['path']).fetch();

      return files
        .map((file: IContentDocument) => ({
          path: file.path
            .replace(/\/index$/, '/')
            .replace(/(\/+)/g, '/')
            .replace(/\/$/, ''),
        }))
        .filter((file: IContentDocument) => !/\/_/.test(file.path));
    },

    fallback: '404.html',
  },
};

export default config;
