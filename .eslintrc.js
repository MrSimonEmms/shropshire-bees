module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [['@', '.']],
        extensions: ['.vue', '.js', '.ts'],
      },
    },
  },
  extends: ['@nuxtjs/eslint-config-typescript', 'plugin:prettier/recommended', '@vue/airbnb'],
  plugins: [],
  // add your custom rules here
  rules: {},
};
