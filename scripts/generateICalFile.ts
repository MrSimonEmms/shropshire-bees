import fs from 'fs';
import icalGenerator, { ICalEventData, ICalEventStatus, ICalCalendarMethod } from 'ical-generator';
import Calendar from '../src/store/calendar';
import Apiary from '../src/store/apiary';

const fileName = './dist/calendar.ics';
const now = new Date();
const startDate = new Date(now.getFullYear(), 0, 1);
// Set to end of next year so events don't stop in December and we miss January events
const endDate = new Date(now.getFullYear() + 1, 11, 31, 23, 59, 59, 999);

const ical = icalGenerator({
  name: 'Shropshire Beekeepers Association',
  description: 'Diary of Shropshire Beekeepers Association',
  timezone: 'Europe/London',
  ttl: 60 * 60 * 24, // 1 day
  method: ICalCalendarMethod.PUBLISH,
  url: 'https://shropshirebees.co.uk/about/calendar',
});
const apiary = new Apiary({});
const cal = new Calendar({});

cal.items(startDate, endDate).forEach((item) => {
  if (!item.timed) {
    // Add one day to end date. All day events seem to treat a "<" not "<="
    item.end.setHours(24);
  }

  const event: ICalEventData = {
    start: item.start,
    end: item.end,
    description: item.description,
    summary: item.name,
    allDay: !item.timed,
    url: 'https://shropshirebees.co.uk/about/calendar',
    timezone: 'Europe/London',
    lastModified: new Date(),
    status: ICalEventStatus.CONFIRMED,
  };

  if (item.location) {
    event.location = item.location.join('\n');
  } else {
    /* Event at SBKA */
    event.location = apiary.address.join('\n');
  }

  ical.createEvent(event);
});

try {
  fs.unlinkSync(fileName);
} catch {
  console.log("File doesn't exist");
}

fs.writeFileSync(fileName, ical.toString());
